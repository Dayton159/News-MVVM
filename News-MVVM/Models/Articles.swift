//
//  Articles.swift
//  News-MVVM
//
//  Created by Dayton on 13/05/21.
//

import Foundation

struct ArticleLists:Decodable {
    let articles: [Article]
}

struct Article:Decodable {
    let title:String
    let description:String?
}
