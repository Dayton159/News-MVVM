//
//  ArticleViewModel.swift
//  News-MVVM
//
//  Created by Dayton on 13/05/21.
//

import Foundation

//MARK: - Parent ViewModel
/* Useful if your view is consist along with segmented control or other UI to be
  represented by the parent viewmodel*/

struct ArticleListViewModel {
    let articles: [Article]
}

extension ArticleListViewModel {
    var numberOfSections:Int {
        return 1
    }
    
    func numberOfRowsInSection (_ section:Int) -> Int {
        return self.articles.count
    }
    
    func articleAtIndex(_ index:Int) -> ArticleViewModel {
        let article = self.articles[index]
        
        return ArticleViewModel(article)
    }
}



//MARK: - Child ViewModel
struct ArticleViewModel{
    private let article:Article
}

//MARK: - Initializers
extension ArticleViewModel {
    init(_ article:Article) {
        self.article = article
    }
}

//MARK: - Display Logic
extension ArticleViewModel {
    
    var title:String {
        return self.article.title
    }
    
    var description:String? {
        return self.article.description
    }
}
