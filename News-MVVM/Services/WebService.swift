//
//  WebService.swift
//  News-MVVM
//
//  Created by Dayton on 13/05/21.
//

import Foundation

class WebService {
    
    static func getArticles(urlString: String, completion: @escaping ([Article]?) -> ()) {
         
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            }
            guard let safeData = data else { return }
            
            do {
                let articles = try JSONDecoder().decode(ArticleLists.self, from: safeData)
               
                DispatchQueue.main.async {
                    completion(articles.articles)
                }
            } catch {
                print(error)
                
            }
            
        }.resume()
    }
}
