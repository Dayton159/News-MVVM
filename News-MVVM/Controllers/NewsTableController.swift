//
//  NewsTableController.swift
//  News-MVVM
//
//  Created by Dayton on 13/05/21.
//

import Foundation
import UIKit

class NewsTableController:UITableViewController {
    
    private var articleListViewModel: ArticleListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let urlString = "https://newsapi.org/v2/top-headlines?country=us&apiKey=c54e8d1dcde540358a495a35a55e655b"
        
        WebService.getArticles(urlString: urlString) { articles in
            
            guard let safeArticles = articles else { return }
            self.articleListViewModel = ArticleListViewModel(articles: safeArticles)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.articleListViewModel == nil ? 0 : self.articleListViewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleListViewModel.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as? ArticleCell else {
            fatalError("ArticleTableViewCell not found")
        }
       let articleViewModel = self.articleListViewModel.articleAtIndex(indexPath.row)
        
        cell.titleLabel.text = articleViewModel.title
        cell.descriptionLabel.text = articleViewModel.description
    
        return cell
    }
}
