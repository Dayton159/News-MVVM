//
//  ArticleCell.swift
//  News-MVVM
//
//  Created by Dayton on 13/05/21.
//

import Foundation
import UIKit

class ArticleCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel!
    
    
}
